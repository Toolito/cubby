var express = require('express'),
  path = require('path'),
  expressJwt = require('express-jwt'),
  User = require('./models/User').User,
  users = require('./models/User'),
  Plan = require('./models/Plan').Plan,
  plans = require('./models/Plan'),
  files = require('./models/File'),
  folders = require('./models/Folder'),
  links = require('./models/Link');

var app = express();
var secret = 'b3Pr0j3ct6325';
var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization');

  next();
}

app.use(express.static(path.join(__dirname, 'app')))
  .use('/api', expressJwt({secret: secret}))
  .use(express.json())
  .use(express.urlencoded())
  //USERS
  .get('/api/users', users.findAll)
  .get('/api/users/:id', users.findById)
  .post('/register', users.register)
  .get('/api/profile', users.findMe)
  .put('/api/profile', users.updateMe)
  .post('/api/users', users.addUser)
  .put('/api/users/:id', users.updateUser)
  .post('/authenticate', users.authenticate)
  .post('/subscribe/:id', users.subscribe)
  .delete('/api/users/:id', users.deleteUser)
  //PLANS
  .get('/api/plans', plans.findAll)
  .get('/plans', plans.findAll)
  .get('/api/plans/:id', plans.findById)
  .post('/api/plans', plans.addPlan)
  .put('/api/plans/:id', plans.updatePlan)
  .delete('/api/plans/:id', plans.deletePlan)
  //FILES
  .get('/api/files', files.findMine)
  .get('/api/files/:id', files.findById)
  .post('/api/files/upload', files.upload)
  .get('/api/files/download/:id', files.download)
  .delete('/api/files/:id', files.removeFile)
  //FOLDER
  .get('/api/folders', folders.findMine)
  .get('/api/folders/:id', folders.findById)
  .get('/api/folders/path/:path', folders.findByPath)
  .put('/api/folders/move/:id', folders.moveFolder)
  .put('/api/folders/:id', folders.updateFolder)
  .post('/api/folders', folders.addFolder)
  .delete('/api/folders/:id', folders.removeFolder)
  //SHARE FOLDERS
  .put('/api/folders/share/:id', folders.share)
  .get('/api/folders/share/ro', folders.findSharedRo)
  .get('/api/folders/share/rw', folders.findSharedRw)
  //LINKS
  .post('/api/links', links.addLink)
  .get('/api/links', links.findMine)
  .get('/links/:secret', links.download)
;


app.listen(3389);
console.log('Listening on port 3389');

