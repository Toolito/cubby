# Cubbyhole API #

### Pré-requis ###

* NodeJS installé
* MongoDB installé

### Installation ###

* cloner le projet

```
#!shell

cd /path/to/folder

node server.js
```


### Routes ###

#### Accès à tous ####

* POST /register (params: name, email, password)
* POST /authenticate (params: email, password)
----
* GET /links/:secret (télécharger un fichier partagé)
 
#### Accès par token ####

* GET /api/profile (récupérer son profil)
* PUT /api/profile (màj son profil)
----
* POST /api/files/upload (params: path, file[])
* GET /api/files/download/:id (télécharger un fichier par id)
----
* GET /api/files (récupérer tout ses fichiers)
* GET /api/files/:id (récupérer un fichier par id)
* DELETE /api/files/:id (supprimer un fichier par id)
----
* GET /api/folders (récupérer tout ses dossiers)
* GET /api/folders/:id (récupérer un dossier par id)
* GET /api/folders/path/:path (récupérer un dossier par path)
* PUT /api/folders/move/:id (params: path ; déplacer un dossier vers path)
* PUT /api/folders/:id (params: folder params ; modifier un dossier) 
* POST /api/folders (params: name, path ; créer un dossier)
* DELETE /api/folders/:id (supprimer un dossier)
----
* PUT /api/folders/share/:id (params : id (userId), permission ('ro', 'rw', 're', 'rew') ; partager un dossier avec un autre utilisateur)
* GET /api/folders/share/ro (récupérer les dossiers partagés read only)
* GET /api/folders/share/rw (récupérer les dossiers partagés read write)
----
* POST /api/links (params: id (du fichier à partager) ; générer un lien de partage fichier)
* GET /api/links (récupérer ses liens de partage)


#### Accès par token admin ####

Simple CRUD sur les users et les plans :

* GET /api/users
* GET /api/users/:id
* POST /api/users
* PUT /api/users/:id
* DELETE /api/users/:id
----
* GET /api/plans
* GET /api/plans/:id
* POST /api/plans
* PUT /api/plans/:id
* DELETE /api/plans/:id