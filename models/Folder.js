var mongoose    = require('./getMongoose.js').mongoose,
    User = require('./User').User,
    users = require('./User'),
    path = require('path'),
    _ = require('underscore'),
    fse = require('fs-extra');

FolderSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    path: {
        type: String,
        required: true
    },
    files: [{ type: mongoose.Schema.ObjectId, ref: 'File'}],
    owner: {
        type: mongoose.Schema.ObjectId,
        required: true,
        ref: 'User'
    },
    guestsRead: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
    guestsWrite: [{ type: mongoose.Schema.ObjectId, ref: 'User'}]
}),
    FolderModel = mongoose.model('Folder', FolderSchema);
exports.Folder = FolderModel;

exports.findByPath = function(req, res){
  var path = req.params.path;
  if(path != ':'){
    path = path.replace('.', '/');
    FolderModel.find({'path': path, owner: req.user.id })
      .populate('files')
      .exec(function(err, folders){
        if(err) res.send(401, err);
        res.send(folders);
      });
  } else {
    FolderModel.findOne({'path': path, owner: req.user.id })
      .populate('files')
      .exec(function(err, folder){
        if(err) res.send(401, err);
        res.send(folder);
      });
  }
};

exports.findMine = function(req, res){
  var userId = req.user.id;
  FolderModel.find({owner: userId})
    .populate('files')
    .exec(function(err, folders){
    if (err) res.send(401, err);
    res.send(folders);
  })
};

exports.findSharedRo = function(req, res){
  var userId = req.user.id;
  FolderModel.find({
    'guestsRead': userId
  })
  .populate('files owner')
  .exec(function(err, folders){
      if(err) res.send(err);
      res.send(folders);
  });
};

exports.findSharedRw = function(req, res){
  var userId = req.user.id;
  FolderModel.find({
    'guestsWrite': userId
  })
    .populate('files owner')
    .exec(function(err, folders){
      if(err) res.send(err);
      res.send(folders);
    });
}


Array.prototype.contains = function(k) {
  for(var i=0; i < this.length; i++){
    if(this[i] == k){
      return true;
    }
  }
  return false;
}

exports.share = function(req, res){

  var folderId = req.params.id;
  var userId = req.body.id;
  var permission = req.body.permission;
  console.log(req.user.email + ' is calling folder share '+req.params.id+ ' '+permission);
  if(permission == 'ro'){
    FolderModel.findById(folderId)
      .exec(function(err, folder){
        if(err) res.send(401, err);
        folder.guestsRead.push(userId);
        folder.save();
        res.send(200, folder);
      });
  } else if(permission == 'rw'){
    FolderModel.findById(folderId)
      .exec(function(err, folder){
        if(err) res.send(401, err);
        folder.guestsWrite.push(userId);
        folder.save();
        res.send(200, folder);
      });
  }else if(permission == 're') {
    FolderModel.findOne({
      '_id': folderId,
      'guestsRead': userId
    })
      .exec(function(err, folder){
        if(err) res.send(401, err);
        folder.guestsRead = _.filter(folder.guestsRead, function(item){
          return item != userId
        });
        folder.save();
        res.send(200, folder);
      });
  } else if (permission == 'rew'){
    FolderModel.findOne({
      '_id': folderId,
      'guestsWrite': userId
    })
      .exec(function(err, folder){
        if(err) res.send(401, err);
        folder.guestsWrite = _.filter(folder.guestsWrite, function(item){
          return item != userId
        });
        folder.save();
        res.send(200, folder);
      });
  }else {
      res.send(401, 'permission parameter is re, ro or rw');
    }

};



exports.findById = function(req, res){
  console.log(req.user.email + ' is calling folder findById '+req.params.id);
  var id = req.params.id;
  FolderModel.findById(id)
    .populate('files')
    .exec(function(err, folder){
      if(err) res.send(401, err);
      res.send(folder);
  });
};

exports.addFolder = function(req, res){
  console.log(req.user.email + ' is calling addFolder');
  var path = req.body.path;
  var name = req.body.name;
  var userId = req.user.id;
  FolderModel.count({
    name: name,
    path: path
  }, function(err, count){
    if(err) res.send(401, err);
    if(count == 0){
      FolderModel.create({
        owner: userId,
        name: name,
        path: path
      }, function(err, folder){
        res.send(200, folder);
      });
    }
  });
};

exports.updateFolder = function(req, res){
  console.log(req.user.email + ' is calling updateFolder');

  FolderModel.findById(req.params.id)
    .populate('files')
    .exec(function(err, folder){
      if(err) res.send(401, err);

      if (req.body.name != folder.name){
        var oldName = folder.name;
        var newName = req.body.name;
        var tabPath = folder.path.replace(/\W/g, '').split('/');
        tabPath = tabPath.filter(Boolean);
        tabPath.push(oldName);
        var childPath = tabPath.join('/');
        console.log(childPath);
        FolderModel.find({
          path : new RegExp(childPath+'(.*)', "i")
        }, function(err, folders){
          folders.forEach(function(childFolder){
            console.log('avant : '+ childFolder.name + ' ' + childFolder.path);
            var newChildPathTab = childFolder.path.split('/');
            console.log(newChildPathTab);
            console.log(oldName + ' '+ newName)
            var i = newChildPathTab.indexOf(oldName);
            newChildPathTab[i] = newName;
            console.log(newChildPathTab);
            var newChildPath = newChildPathTab.join('/');
            childFolder.path = newChildPath;
            childFolder.save();
            console.log('apres : ' +childFolder.name + ' ' + childFolder.path);
          });
        });
        folder.name = newName;
      }
      if (typeof req.body.guestsRead == 'array' && req.body.guestsRead > 0 )
      {
        req.body.guestsRead.forEach(function(user){
          folder.guestsRead.push(user);
        });
      }
      if (typeof req.body.guestsWrite == 'array' && req.body.guestsWrite > 0 )
      {
        req.body.guestsWrite.forEach(function(user){
          folder.guestsWrite.push(user);
        });
      }
      folder.files = req.body.files;
      folder.save();
      res.send(200, 'Folder updated');
    });
};

exports.moveFolder = function(req, res){
  FolderModel.findById(req.params.id, function(err, folder){
    if (err) res.send(401, err);
    var newPath = req.body.path;
    if(newPath == "#"){
      newPath = '';
    }
    var newTabPath = newPath.split('/');
    var tabPath = folder.path.replace(/\W/g, '').split('/');
    tabPath = tabPath.filter(Boolean);
    tabPath.push(folder.name);
    var childPath = tabPath.join('/');
    FolderModel.find({
      path : new RegExp(childPath+'(.*)', "i")
    }, function(err, folders){
      folders.forEach(function(childFolder){
        console.log('avant : '+ childFolder.name + ' ' + childFolder.path);
        var newChildPathTab = newTabPath.slice(0);
        newChildPathTab.push(folder.name);
        newChildPathTab = newChildPathTab.filter(Boolean);
        console.log(newChildPathTab);
        var newChildPath = newChildPathTab.join('/');
        childFolder.path = newChildPath;
        childFolder.save();
        console.log('apres : ' +childFolder.name + ' ' + childFolder.path);
      });
      if(newPath == ''){
        folder.path = '#';
      } else {
        folder.path = newPath;
      }
      folder.save();
      res.send(200, 'Chemin changé');
    });
  });
};

exports.removeFolder = function(req, res){
  console.log(req.user.email + ' is calling removeFolder');
  FolderModel.findById(req.params.id)
    .populate('files')
    .exec(function(err, folder){
      if (err) res.send(401, err);

      var tabPath = folder.path.replace(/\W/g, '').split('/');
      tabPath = tabPath.filter(Boolean);
      tabPath.push(folder.name);
      var childPath = tabPath.join('/');
      User.findById(req.user.id)
        .exec(function(err, user){
          if(err) res.send(401, err);
          FolderModel.find({
            path : new RegExp(childPath+'(.*)', "i")
          }, function(err, folders){
            folders.forEach(function(childFolder){
              console.log(childFolder.name)
              childFolder.files.forEach(function(childFile){
                var filePath = path.resolve(__dirname + '/../public/' + req.user.id + '/' + childFile.name);

                fse.remove(filePath, function(err){
                  if(err) res.send(401, err);
                  user.storage -= childFile.size;
                  user.save();

                  childFile.remove();
                  console.log('Fichier supprime');
                });
              });
              childFolder.remove();
            });
            folder.files.forEach(function(file){
              var filePath = path.resolve(__dirname + '/../public/' + req.user.id + '/' + file.name);

              fse.remove(filePath, function(err){
                if(err) res.send(401, err);
                console.log(user);
                console.log(file);
                user.storage -= file.size;
                user.save();

                file.remove();
                console.log('Fichier supprime');
              });
            })
            folder.remove();
            res.send(200, 'Dossier(s) supprime(s)');
          });
      });

    });
};