var mongoose    = require('./getMongoose.js').mongoose,
  PlanSchema = mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    price: {
      type: Number,
      required: true
    },
    duration: {
      type: Number,
      required: true
    },
    storageSpace: {
      type: Number,
      required: true
    },
    maxBandwidth: {
      type: Number,
      required: true
    },
    transferQuota: {
      type: Number,
      required: true
    }
  }),
  PlanModel = mongoose.model('Plan', PlanSchema);
exports.Plan = PlanModel;


exports.findById = function(req, res) {
  var id = req.params.id;
  console.log('Retrieving plan: ' + id);
  PlanModel.findById(id, function(e,r){
    if(r != null){
      res.send(r);
    }
    else{
      console.log('User not found');
      res.send(null);
    }
  })
};



exports.findAll = function(req, res) {
  PlanModel.find(function (err, plans){
    if(err) {res.send({'error':'An error has occured'}); }
    res.send(plans);
  });
};

exports.addPlan = function(req, res, callback) {
  var plan = new PlanModel(req.body);
  console.log('Adding plan!');
  plan.save( function(err) {
    if(err) {res.send(err);}
    res.send('item saved');
  });
}

exports.updatePlan = function(req, res, callback) {
  console.log('admin is calling updatePlan')
  var id = req.params.id;
  var plan = req.body;
  PlanModel.findOne({'_id':id}, function(e,r){
    if(r != null){
      PlanModel.update({'_id':id}, plan, function(err){
        if(err) {console.log(err);}
        res.send('item updated');
      });
    }
    else{
      res.send('Plan not found');
    }
  });
}

exports.deletePlan = function(req, res, callback) {
  var id = req.params.id;
  PlanModel.remove({'_id':id}, function(err, result){
    if(err){
      res.send({'error':'An error has occured - '+ err});
    } else {
      console.log(result + 'document(s) deleted');
      res.send(result + 'document(s) deleted');
    }
  })
}
