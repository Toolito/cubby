var jwt = require('jsonwebtoken'),
  bcrypt = require('bcryptjs'),
  mongoose    = require('./getMongoose.js').mongoose,
  Plan = require('./Plan').Plan,
  Datenow = new Date(),
  UserSchema = mongoose.Schema({
    name: {
      type: String,
      required: true,
      unique: true
    },
    email: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true,
      select: false
    },
    createdAt: {
      type: Date,
      default: Date.now
    },
    startPlan: {
      type: Date,
      default: Date.now
    },
    endPlan: {
      type: Date,
      default: Datenow.setDate(Datenow.getDate() + 365)
    },
    quota: {
      type: Number,
      default: 0
    },
    storage: {
      type: Number,
      default: 0
    },
    admin: {
      type: Boolean,
      default: false,
      select: false
    },
    plan: {
      type: mongoose.Schema.ObjectId,
      ref: 'Plan'
    }
  });

UserSchema.pre('save', function(next) {
  var user = this;

  if (!user.isModified('password')) return next();

  bcrypt.genSalt(10, function(err, salt){
    if (err) return next(err);

    bcrypt.hash(user.password, salt, function(err, hash){
      if (err) return next(err);

      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.comparePassword = function(candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch){
    if (err) return callback(err);
    callback(null, isMatch);
  });
};

UserModel = mongoose.model('User', UserSchema);
exports.User = UserModel;

//Accessible par un utilisateur lambda
exports.authenticate = function(req, res){
  var email = req.body.email;
  var pass = req.body.password;
  UserModel.findOne({'email': email})
    .select("+password +admin")
    .exec(function (e, r) {
      if(r != null){
        var token = "";
        bcrypt.compare(pass, r.password, function(err, test) {
          if(test === true){
            if(r.admin) {
              console.log('Auth admin');
              var profile = {
                name: r.name,
                email: r.email,
                id: r._id,
                admin: r.admin
              };
            } else {
              console.log('Auth user');

              var profile = {
                name: r.name,
                email: r.email,
                id: r._id
              }
            }
            token = jwt.sign(profile, 'b3Pr0j3ct6325', { expiresInMinutes: 60*3});
            res.json({token: token, uid: r._id});
          } else {
            res.send(401, 'Wrong user or password');
          }
        });
      }
      else
        res.send(401, 'Wrong user or password');
    });
};

exports.register = function(req, res, callback) {
  var user = new UserModel(req.body);
  Plan.findOne({name: 'Free'}, function( err, freePlan){
    user.plan = freePlan._id;
  });

  user.save( function(err) {
    if(err) throw(err);
    var profile = {
      name: user.name,
      email: user.email
    };
    var token = jwt.sign(profile, 'b3Pr0j3ct6325', { expiresInMinutes: 60*3});
    res.json({token: token, uid: res._id});
  });
};


exports.findMe = function(req, res) {
  console.log(req.user.email + ' is calling findMe');
  UserModel.findById(req.user.id)
    .populate('plan')
    .select('+admin')
    .exec( function(e, r){
      if(r != null){
        res.send(r);
      } else {
        res.send(401, 'User (by id) not found');
      }
    });
}

exports.updateMe = function(req, res, callback) {
  console.log(req.user.email + ' is calling updateMe');
  var user = req.body;
  UserModel.findById(req.user.id)
    .populate('plan')
    .exec(function(e, r){
    if(r != null && !r.admin){
      r.name = user.name;
      r.email = user.email;
      r.save();

      res.send(200, r);
    } else {
      res.send(401, 'User not found');
    }
  })
}

exports.subscribe = function(req, res, callback) {
  UserModel.findById(req.body.userId, function(e, user){
    if(r != null){
      Plan.findById(req.body.planId, function(e, plan){
        if(r != null){
          if (req.body.delay == 'y'){
            user.endPlan = user.endPlan + 365;
          } else if(req.body.delay == 'm') {
            user.endPlan = user.endPlan + 30;
          }
          user.plan = plan._id;
          user.save();
        } else {
          res.send(401, 'Plan not found');
        }
      });
    }else{
      res.send(401, 'User not found');
    }
  });
}

//Accessible par l'admin

exports.findById = function(req, res) {
  if(!req.user.admin) return res.send(401);
  var id = req.params.id;
  console.log(req.user.email + ' is calling /api/users/'+id);
  console.log('Retrieving user: ' + id);
  UserModel.findById(id, function(e,r){
    if(r != null){
      res.send(r);
    }
    else{
      res.send(401, 'User not found');
    }
  })
};


exports.findAll = function(req, res) {
  UserModel.find({
    email: {$ne : req.user.email}
  })
    .exec(function (err, users){
    if(err)
      res.send(401, 'User not found');
    else
      res.send(users);
  });
};

exports.addUser = function(req, res, callback) {
  if(!req.user.admin) return res.send(401);

  var user = new UserModel(req.body);
  console.log('Adding user!');
  user.save( function(err) {
    if(err)
      res.send(401, 'Error inserting a user');
    else
      res.send('User added !');
  });
};



exports.updateUser = function(req, res, callback) {
  if(!req.user.admin) return res.send(401);

  var id = req.params.id;
  console.log(req.user.email + ' is calling /api/users/'+id+' UPDATE');

  var user = req.body;
  UserModel.findOne({'_id':id}, function(e,r){
    if(r != null && !r.admin){
      UserModel.update({'_id':id}, user, function(err){
        if(err)
          res.send(401,'Error updating a user');
        else
          res.send('User updated');
      });
    }
    else{
      res.send(401, 'User not found');
    }
  });
}

exports.deleteUser = function(req, res, callback) {
  if(!req.user.admin) return res.send(401, 'You are an idiot');

  var id = req.params.id;
  console.log(req.user.email + ' is calling /api/users/'+id+' DELETE');

  UserModel.remove({'_id':id}, function(err, result){
    if(err)
      res.send(401, 'Cannot remove this user');
    else
      res.send('User deleted');
  })
}
