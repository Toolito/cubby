var mongoose    = require('./getMongoose.js').mongoose,
  formidable = require('formidable'),
  fse = require('fs-extra'),
  util = require('util'),
  Folder = require('./Folder').Folder,
  User = require('./User').User,
  users = require('./User'),
  path = require('path'),
  _ = require('underscore');

  FileSchema = mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    type: {
        type: String,
        required: true
    },
    size: {
      type: Number,
      required: true
    },
    lastModifiedDate: {
      type: Date,
      required: true
    },
    owner: {
      type: mongoose.Schema.ObjectId,
      required: true,
      ref: 'User'
    },
    guestsRead: [{ type: mongoose.Schema.ObjectId, ref: 'User' }],
    guestsWrite: [{ type: mongoose.Schema.ObjectId, ref: 'User'}]
  }),
  FileModel = mongoose.model('File', FileSchema);
exports.File = FileModel

exports.findById = function(req, res) {
  var id = req.params.id;
  var userId = req.user.id;
  console.log(req.user.email + ' is calling /api/files/'+id);
  User.findById(userId, function(e, user){
    if(user != null){
      FileModel.find({
        '_id': id,
        'owner': user
      }, function(err, file){
        if(err) res.send(401, err);
        res.send(file);
      });
    } else {
      res.send(401, 'User not found');
    }
  })
};

exports.findMine = function(req, res) {
  var userId = req.user.id;
  User.findById(userId, function(err, user){
    if(user != null){
      FileModel.find({
        'owner': user
      }, function(err, files){
        if(err) res.send(401, err);
        res.send(files);
      });
    } else {
      res.send(401, 'User not found');
    }
  });
};

exports.upload = function(req, res) {

  var form = new formidable.IncomingForm(),
    files = [],
    fields = [],
    response = '',
    gUser;

  User.findById(req.user.id)
    .populate('plan')
    .exec(function(err, user){
      gUser = user;
      form.uploadDir = '/tmp/';
      form.encoding = 'binary';
      form.maxBandwidth = user.plan.maxBandwidth * 1024 * 1024; // 100 KO ici = 600 KO réels
      form.parse(req);
    });

  form
  .on('field', function(field, value) {
    //console.log(field, value);
    fields.push([field, value]);
  })
  .on('file', function(field, file) {
    console.log(file.size + gUser.storage +' '+ gUser.plan.storageSpace * 1024 * 1024 * 1024);
    if(file.size + gUser.storage >= gUser.plan.storageSpace * 1024 * 1024 * 1024)
    {
      var currentSpaceMo = gUser.storage / 1024 / 1024;
      var planStorageSpaceMo = gUser.plan.storageSpace * 1024 * 1024;
      response = 'Transfer quota is reached, '+ currentSpaceMo + '/' +planStorageSpaceMo +' Mo ';
    } else{
      console.log(field, file);
      var temp_path = file.path;
      var file_name = file.name;
      var new_location = 'public/' + req.user.id +'/';
      //console.log(temp_path +' '+ file_name)

      fse.move(temp_path, new_location + file_name, function(err) {
        if (err) {
          console.error(err);
        } else {
          console.log("success!");
        }
      });
      file.path = req.body.path;
      files.push([field, file]);
    }
  })
  .on('progress', function(bytesReceived, bytesExpected) {
    var percent_complete = (bytesReceived / bytesExpected) * 100;
      console.log(percent_complete.toFixed(2));
  })
  .on('end', function(vitesse) {
    console.log('VITESSE : '+ vitesse);
    if(files.length > 0){
      files.forEach(function(file){
        console.log(file[1]);
        FileModel.create({
          name: file[1].name,
          size: file[1].size,
          type: file[1].type,
          lastModifiedDate: file[1].lastModifiedDate,
          owner: gUser
        }, function (err, newFile) {
          if (err){ console.log(err); }
          else{
            if(typeof fields[0] != 'undefined' && fields[0][0] == 'id'){ //luploader nest pas owner
              Folder.findById(fields[0][1])
                .populate('owner')
                .exec(function(err, folder){
                  if(err) res.send(401, err);
                  folder.owner.storage += newFile.size;
                  folder.files.push(newFile);
                  folder.owner.save();
                  folder.save();
                });
            } else if(typeof fields[0] != 'undefined' && fields[0][0] == 'path'){
              gUser.storage += file[1].size;
              gUser.save();
              console.log('PATH : '+ fields[0][1]);
              if(fields[0][1].length != 0){
                var folderStringTab = fields[0][1].split('/');
                folderStringTab = folderStringTab.filter(Boolean); //f1/f2/f3
                var folderStringTabr = folderStringTab.slice(0);
                folderStringTabr.reverse();
                folderStringTabr.forEach(function(folderString, index){
                  var newArray = _.without(folderStringTab,folderString);
                  var newPath = newArray.join('/');
                  folderStringTab = _.without(folderStringTab, folderString);
                  if (newArray == ''){
                    newArray = '#';
                  }
                  if (newPath == ''){
                    newPath = '#';
                  }
                  if (index == 0){ //Le dernier element doit contenir les fichiers
                    Folder.findOne({
                      name: folderString,
                      path: newPath
                    }, function(err, folder){
                      if(folder) { //si il existe on push et update
                        folder.files.push(newFile);
                        folder.save();
                      } else { //sinon on le crée et on push
                        Folder.create({
                          name: folderString,
                          files: [newFile],
                          path: newArray,
                          owner: gUser
                        });
                      }
                    })
                  } else if(index == folderStringTabr.length -1){ //premier element root #
                    Folder.count({
                      name: folderString,
                      path: '#'
                    }, function(err, count){
                      if(count == 0){
                        Folder.create({
                          name: folderString,
                          files: [],  //premier element ne contient pas de fichiers
                          path: '#',
                          owner: gUser
                        });
                      }
                    })
                  } else { //lelement est au milieu du tableau
                    Folder.count({
                      name: folderString,
                      path: newPath
                    }, function(err, count){
                      if (count == 0){
                        Folder.create({
                          name: folderString,
                          files: [],
                          path: newPath,
                          owner: gUser
                        });
                      }
                    })
                  }
                });
              }else {
                Folder.findOne({
                  name: '#',
                  path: ':',
                  owner: gUser
                }, function(err, folder){
                  if (err){ console.log(err); }
                  else if (!folder) {
                    Folder.create({
                      name: '#',
                      path: ':',
                      files: [newFile],
                      owner: gUser
                    });
                  } else {
                    folder.files.push(newFile);
                    folder.save();
                  }
                })
              }
            } else {
              res.send(401, 'specify path or id');
            }
          }
        });
      });
      res.writeHead(200, {'content-type': 'text/plain'});
      res.write('received fields:\n\n '+util.inspect(fields));
      res.write('\n\n');
      res.end('received files:\n\n '+util.inspect(files));
    } else {
      res.send(401, response);
    }
  });

};

exports.download = function(req, res){
  FileModel.findById(req.params.id, function(err, file){
    if(err) res.send(401, 'fichier non trouvé');
    var myFile = path.resolve(__dirname + '/../public/'+req.user.id+'/'+file.name);
    res.download(myFile);
  })
};

exports.updateFile = function(req, res){
  FileModel.findById(req.params.id, function(err, file){
    if(err) res.send(401, 'fichier non trouvé');

    if (req.body.path){
      file.path = req.body.path;
    }
    if (typeof req.body.guestsRead == 'array' && req.body.guestsRead > 0 )
    {
      req.body.guestsRead.forEach(function(user){
        file.guestsRead.push(user);
      });
    }
    if (typeof req.body.guestsWrite == 'array' && req.body.guestsWrite > 0 )
    {
      req.body.guestsWrite.forEach(function(user){
        file.guestsWrite.push(user);
      });
    }

    file.save();
  });
};

exports.removeFile = function(req, res){
  console.log(req.user.email + ' is calling removeFile');
  User.findById(req.user.id)
    .exec(function(err, user){
      if(err) res.send(401, err);

      FileModel.findById(req.params.id)
        .exec(function(err, file){
          if(err) res.send(401, err);

          var filePath = path.resolve(__dirname + '/../public/' + req.user.id + '/' + file.name);

          fse.remove(filePath, function(err){
            if(err) res.send(401, err);

            user.storage -= file.size;
            user.save();

            file.remove();
            res.send(200, 'Fichier supprime');
          });
        });
    });

}