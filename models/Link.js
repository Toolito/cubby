var mongoose    = require('./getMongoose.js').mongoose,
  chance = require('chance').Chance(),
  User = require('./User').User,
  path = require('path');

LinkSchema = mongoose.Schema({
  secret: {
    type: String,
    required: true,
    unique: true
  },
  quota: {
    type: Number,
    default: 0
  },
  owner: {
    type: mongoose.Schema.ObjectId,
    required: true,
    ref: 'User'
  },
  file: {
    type: mongoose.Schema.ObjectId,
    required: true,
    ref: 'File'
  }
}),
  LinkModel = mongoose.model('Link', LinkSchema);
exports.Link = LinkModel


exports.findMine = function(req, res){
  var userId = req.user.id;
  LinkModel.find({owner: userId})
    .exec(function(err, links){
      if (err) res.send(401, err);
      res.send(links);
    });
};

exports.addLink = function(req, res){
  console.log(req.user.email +' is calling addLink');
  var userId = req.user.id;
  var fileId = req.body.id;
  var random = chance.string({pool: 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN1234567890'});
  LinkModel.count({
    secret: random
  }, function(err, count){
    console.log(count);
    if(err) res.send(401, err);
    if(count == 0){
      LinkModel.create({
        owner: userId,
        file: fileId,
        secret: random
      }, function(err, link){
        if(err) res.send(401, err);
        res.send(200, link);
      });
    }else {
      this.addLink(req, res);
    }
  });
};

exports.download = function(req, res){
  var secret = req.params.secret;
  LinkModel.findOne({secret: secret})
    .populate('file owner')
    .exec(function(err, link){
      if (err) res.send(401, err);
      var myFile = path.resolve(__dirname + '/../public/'+link.owner._id+'/'+link.file.name);
      res.download(myFile);
      User.findById(link.owner._id, function(err, user){
        if (err) res.send(401, err);
        user.quota += link.file.size;
        user.save();
      })
    });
};

